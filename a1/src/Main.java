public class Main {
    public static void main(String[] args) {

        Phonebook newPhonebook = new Phonebook();

        Contact contact1 = new Contact("John","09123456789","Somewhere");
        Contact contact2 = new Contact("Jane","09112233445","Anywhere");

        newPhonebook.setContacts(contact1);
        newPhonebook.setContacts(contact2);

        if(!newPhonebook.getContacts().isEmpty()){
            newPhonebook.getContacts().forEach(element ->{
                System.out.println("--------------------");
                System.out.println(element.getName() +" has the following registered number:");
                System.out.println(element.getContactNumber());
                System.out.println(element.getName() +" has the following registered address:");
                System.out.println(element.getAddress());
            });
        } else {
            System.out.println("The phonebook is currently empty.");
        }
    }
}