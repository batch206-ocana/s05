public class Parent {

    private String name;
    private int age;

    //polymorphism
    //static polymorphism : this is the ability to have multiple method of the same name but changes form based on the number of arguments or the type

    public Parent() {
    }

    public Parent(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public void greet(){
        System.out.println("Hello friend!");
    }

    public void greet(String name, String timeOfDay){
        System.out.println("Good " +timeOfDay +"!, " +name);
    }
    //dynamic method dispatch/Runtime polymorphism : the ability of a subclass to inherit a method from a parent but override it and change its definition in the sub-class.
    public void introduce(){
        System.out.println("Hi! I'm " +this.name + ". I am a parent.");
    }
}
