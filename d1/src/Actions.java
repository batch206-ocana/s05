public interface Actions {
    //interfaces are used to achieve abstraction. It hides away the actual implementation of the methods and simply shows a "list"/a "menu" of methods that can be done.
    //interfaces are like blueprints for your classes. Because any class that implements our interface MUST have the methods in the interface.
    public void sleep();
    public void run();
}
